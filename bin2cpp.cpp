#include <vector>
#include <string>
#include <map>
#include <set>
#include <iterator>
#include <iostream>
#include <fstream>
#include <iomanip>

void print_usage (const char * program_name)
{
	std::cout << "Usage: " << program_name << " -h\n";
	std::cout << "Usage: " << program_name << " [ <object-name> ]*\n";
	std::cout << "       " << program_name << " -s [ -i <include-file> ]* [ [ -b ] <object-name> <file> ]*\n";
}

void put_chars (std::size_t count, char c)
{
	while (count --> 0)
		std::cout << c;
}

void put_tabs (std::size_t count)
{
	put_chars(count, '\t');
}

void put_spaces (std::size_t count)
{
	put_chars(count, ' ');
}

struct parsed_name_t
{
	std::vector<std::string> namespaces;
	std::string name;
};

parsed_name_t parse_name (std::string const & full_name)
{
	std::vector<std::string> namespaces;

	for (std::size_t pos = 0, prev = 0; (pos = full_name.find("::", prev)), prev != std::string::npos; prev = (pos == std::string::npos) ? std::string::npos : pos + 2)
	{
		namespaces.push_back(full_name.substr(prev, pos - prev));
	}

	std::string name = std::move(namespaces.back());
	namespaces.pop_back();

	return parsed_name_t{ std::move(namespaces), std::move(name) };
}

struct namespace_tree
{
	std::set<std::string> leaves;
	std::map<std::string, namespace_tree> children;
};

struct nop_t
{
	template <typename ... Args>
	void operator () (Args ...)
	{ }
};

template <typename FObject = nop_t, typename FNamespace = nop_t, typename FDelim = nop_t>
void traverse (namespace_tree const & tree, FObject f_object = FObject(), FNamespace f_namespace = FNamespace(), FDelim f_delim = FDelim(), std::size_t depth = 0)
{
	for (auto it = tree.leaves.begin(); it != tree.leaves.end();)
	{
		f_object(*it, depth);

		if (++it != tree.leaves.end())
			f_delim(depth);
	}

	if (!tree.leaves.empty() && !tree.children.empty())
		f_delim(depth);

	for (auto it = tree.children.begin(); it != tree.children.end();)
	{
		f_namespace(it->first, depth, true);
		traverse(it->second, f_object, f_namespace, f_delim, depth + 1);
		f_namespace(it->first, depth, false);

		if (++it != tree.children.end())
			f_delim(depth);
	}
}

int main (int argc, char ** argv)
{
	if (argc < 1)
	{
		print_usage(argv[0]);
		return 0;
	}

	if (argc >= 2 && argv[1] == std::string("-h"))
	{
		print_usage(argv[0]);
		return 0;
	}

	if (!(argc >= 2 && argv[1] == std::string("-s")))
	{
		namespace_tree tree;

		for (int a = 1; a < argc; ++a)
		{
			auto parsed = parse_name(argv[a]);
			namespace_tree * current = &tree;
			for (auto const & ns : parsed.namespaces)
			{
				current = &current->children[std::move(ns)];
			}

			current->leaves.insert(std::move(parsed.name));
		}

		std::cout << "#pragma once\n\n#include <boost/utility/string_ref.hpp>\n\n";

		traverse(tree, [](std::string const & name, std::size_t depth)
		{
			put_tabs(depth);
			std::cout << "extern const boost::string_ref " << name << ";\n";
		}, [](std::string const & name, std::size_t depth, bool in)
		{
			if (in)
			{
				put_tabs(depth);
				std::cout << "namespace " << name << "\n";
				put_tabs(depth);
				std::cout << "{\n";
			}
			else
			{
				put_tabs(depth);
				std::cout << "}\n";
			}
		}, [](std::size_t){ std::cout << "\n"; });
	}
	else
	{
		std::vector<std::string> includes;

		std::map<std::string, std::string> object_map;
		std::map<std::string, bool> object_binary;

		for (int a = 2; a < argc;)
		{
			if (a + 2 > argc)
			{
				print_usage(argv[0]);
				return 0;
			}

			std::string first(argv[a]);
			std::string second(argv[a + 1]);
			a += 2;

			if (first == "-i")
			{
				includes.push_back(std::move(second));
			}
			else
			{
				bool binary = false;

				if (first == "-b")
				{
					if (a >= argc)
					{
						print_usage(argv[0]);
						return 0;
					}
					binary = true;
					first = std::move(second);
					second = std::string(argv[a]);
					++a;
				}

				object_map[first] = std::move(second);
				object_binary[first] = binary;
			}
		}

		for (auto const & i : includes)
			std::cout << "#include <" << i << ">\n";

		if (!includes.empty())
			std::cout << "\n";

		std::cout << "#include <boost/utility/string_ref.hpp>\n\n";

		std::map<std::string, std::string> static_array_map;
		std::size_t max_object_name = 0;
		std::size_t max_array_name = 0;

		{
			for (auto const & p : object_map)
			{
				auto parsed = parse_name(p.first);
				std::string array_name;
				for (auto const & ns : parsed.namespaces)
				{
					array_name += ns;
					array_name += "_";
				}
				array_name += parsed.name;

				if (max_object_name < p.first.size())
					max_object_name = p.first.size();

				if (max_array_name < array_name.size())
					max_array_name = array_name.size();

				static_array_map[p.first] = std::move(array_name);
			}
		}

		std::cout << "namespace\n{\n\n";

		for (auto const & object : object_map)
		{
			if (object_binary[object.first])
			{
				std::cout << "\tchar const " << static_array_map[object.first] << "[] =\n\t{";

				const std::size_t block_size = 0x1000;
				const std::size_t line_size = 0x10;

				std::vector<char> block(block_size);
				std::size_t total = 0;

				std::ifstream ifs(object.second);

				while (ifs)
				{
					ifs.read(block.data(), block.size());

					total += ifs.gcount();
					for (int i = 0; i < ifs.gcount(); ++i)
					{
						if ((i % line_size) == 0)
							std::cout << "\n\t\t";
						std::cout << "0x" << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(block[i]) << ", ";
					}
				}

				std::cout << "\n\t};\n\n";
			}
			else
			{
				std::cout << "\tchar const " << static_array_map[object.first] << "[] = \nR\"(";

				std::ifstream ifs(object.second);
				if (ifs.peek() != std::ifstream::traits_type::eof())
					std::cout << ifs.rdbuf();

				std::cout << ")\";\n\n";
			}
		}

		std::cout << "}\n\n";

		std::cout << "template <std::size_t N>\nstatic boost::string_ref from_array (const char (&a)[N])\n{";
		std::cout << "\n\treturn { a, N };\n}\n\n";

		for (auto const & object : object_map)
		{
			std::cout << "extern const boost::string_ref " << object.first;
			put_spaces(max_object_name - object.first.size());
			std::cout << " = from_array(" << static_array_map[object.first];
			put_spaces(max_array_name - static_array_map[object.first].size());
			std::cout << ");\n";
		}
	}
}
